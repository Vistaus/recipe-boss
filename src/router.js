import Vue from 'vue';
import Router from 'vue-router';

import RecipeList from '@/views/RecipeList';
import RecipeEdit from '@/views/RecipeEdit';
import RecipeView from '@/views/RecipeView';
import GroceryList from '@/views/GroceryList';
import GroceryEdit from '@/views/GroceryEdit';
import GroceryCategories from '@/views/GroceryCategories';

Vue.use(Router);

export default new Router({
    mode: (process.env.VUE_APP_APP_MODE === 'true') ? null : 'history',
    routes: [
        {
            path: '/',
            redirect: { name: 'recipe-list' },
        },
        {
            path: '/recipes',
            name: 'recipe-list',
            component: RecipeList,
        }, {
            path: '/recipes/new',
            name: 'recipe-new',
            component: RecipeEdit,
        }, {
            path: '/recipes/edit/:id',
            name: 'recipe-edit',
            component: RecipeEdit,
        }, {
            path: '/recipes/:id',
            name: 'recipe-view',
            component: RecipeView,
        }, {
            path: '/groceries',
            name: 'grocery-list',
            component: GroceryList,
        }, {
            path: '/groceries/categories',
            name: 'grocery-categories',
            component: GroceryCategories,
        }, {
            path: '/groceries/:id',
            name: 'grocery-edit',
            component: GroceryEdit,
        },
    ],
});
